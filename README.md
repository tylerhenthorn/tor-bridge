zek_tor_bridge
===

This solution is based on https://github.com/vimagick/dockerfiles/tree/master/tor and https://github.com/gtank/obfs4bridge.  

It runs a Tor bridge relay with the obfs4, obfs3, fte, meek pluggable transports, and uses local disk for state and key persistence.  

From https://www.torproject.org:  
> An increasing number of censoring countries are using Deep Packet Inspection (DPI) to classify Internet traffic flows by protocol. While Tor uses bridge relays to get around a censor that blocks by IP address, the censor can use DPI to recognize an d filter Tor traffic flows even when they connect to unexpected IP addresses.  

> Pluggable Transports (PT) transform the Tor traffic flow between the client and the bridge. This way, censors who monitor traffic between the client and the bridge will see innocent-looking transformed traffic instead of the actual Tor traffic.  


## Build
```
docker build -t zek_tor_bridge . 
```

## Create user, certificate and storage
```
sudo useradd zek_tor_bridge 

sudo mkdir -p /var/lib/zek_tor_bridge 

sudo openssl req -x509 \  
    -newkey rsa:2048 \  
    -keyout /var/lib/zek_tor_bridge/key.pem \  
    -out /var/lib/zek_tor_bridge/cert.pem \  
    -nodes 

sudo chown -R zek_tor_bridge:zek_tor_bridge /var/lib/zek_tor_bridge 
```

## Start via systemd
```
sudo bash -c "cat > /etc/systemd/system/zek_tor_bridge.service" <<EOF 
[Unit]
Description=ZekTorBridge
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=30
ExecStartPre=-/usr/bin/docker kill zek_tor_bridge1
ExecStartPre=-/usr/bin/docker rm zek_tor_bridge1
ExecStart=/usr/bin/docker run --user zek_tor_bridge --name zek_tor_bridge1 -p 9001:9001 -p 9080:9080 -p 9081:9081 -p 9082:9082 -p 9083:9083 -v /etc/localtime:/etc/localtime:ro -v /var/lib/zek_tor_bridge:/var/lib/tor zek_tor_bridge

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload

sudo systemctl start zek_tor_bridge
```
